from Compare import CompareWords
from File import Open

#testList = ['abcde', 'fghij', 'klmno', 'pqrst', 'axcye','wvxyz', 'klpno'] Used for testing
boxIds = Open("CheckSumInput.txt")

matchFound = False
for id in boxIds: 
    if matchFound:
        break
    for id2 in boxIds:
        matching = CompareWords(id, id2)
        if(matching is None):
            continue
        else:            
            print(matching)
            matchFound = True
            break