import unittest
from Compare import CompareWords 

class CompareTest(unittest.TestCase):
    def test_more_than_one_letter_diff(self):
        self.assertIs(CompareWords("abcde", "fghij"), 'none')
    def test_exactly_one_diff(self):
        self.assertIs(CompareWords("fghij", "fguij"), 'fgij')        

if __name__ == '__main__':
    unittest.main()
