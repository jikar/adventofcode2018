import os
currentPath = os.path.abspath(os.path.dirname(__file__))
path = os.path.join(currentPath, "CalibrationInput.txt")

with open(path, "r") as f :
    array = []
    for line in f:                        
        n = int(line)        
        array.append(n)

    print(sum(array))