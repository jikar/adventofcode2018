import collections
from collections import Counter
from File import Open

#testList = ['abcdef', 'bababc', 'abbcde', 'abcccd', 'aabcdd', 'abcdee', 'ababab'] - Used for testing
CONST_THREE = 3
CONST_TWO = 2

boxIds = Open("CheckSumInput.txt")
twoTimes = 0
threeTimes = 0

for id in boxIds:                
    c = collections.Counter(id)
    threeAvailable = True
    twoAvailable = True   
    for letter in c:         
         if c[letter] == CONST_TWO and twoAvailable  :
             twoAvailable = False
             twoTimes += 1
         if(c[letter] == CONST_THREE) and threeAvailable :
             threeTimes += 1
             threeAvailable = False

result = twoTimes*threeTimes
print (str(twoTimes) + ' * ' + str(threeTimes))
print (str(result))