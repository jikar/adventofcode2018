import os
from collections import Counter
currentPath = os.path.abspath(os.path.dirname(__file__))
path = os.path.join(currentPath, "CalibrationInput.txt")

with open(path, "r") as f :
    array = []
    for line in f:                        
        n = int(line)        
        array.append(n)

matchNotFound = True
stepByStepFrequenzy = {0}
currentFreq = 0    

while(matchNotFound):
    for c in array:   
        currentFreq += c        
        
        if currentFreq in stepByStepFrequenzy:                        
            print(currentFreq)
            matchNotFound = False
            break
        else:
            stepByStepFrequenzy.add(currentFreq)
