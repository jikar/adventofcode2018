import os
from collections import Counter
import collections

def Open(fileName):
    currentPath = os.path.abspath(os.path.dirname(__file__))
    path = os.path.join(currentPath, fileName)

    content = []
    with open(path, "r") as f :
     for line in f:                                  
        content.insert(0, line.rstrip())

    return content